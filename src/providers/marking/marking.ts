
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

@Injectable()
export class MarkingProvider {
data: any;

totalScore = 0;
numSubject = 0;
numQuestionpersub = 0;

  constructor(public http: Http) {
    console.log('Hello MarkingProvider Provider');
  }

  load(){

		if(this.data){
			return Promise.resolve(this.data);
		}

		return new Promise(resolve => {

			this.http.get('assets/data/result/result.json').map(res => res.json()).subscribe(data => {
				this.data = data.subjects;
				resolve(this.data);
			});

		});

	}

}
