import { Injectable} from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class Data  {
  score: number = 0;
  data: any;
  universityData: any;
  newjason;
  question: any;
  options: any;
  Number: any;
  English: any;
  Mathematics: any;

  constructor(public http: Http,) {
  }
 
  next(){
		
	}
 
  load(){
 
    if(this.data){
        return Promise.resolve(this.data);
    }

    return new Promise(resolve => {

          this.newjason =  this.http.get('assets/data/questions.json').map(res => res.json()).subscribe(data => {
            this.data = data.questions;
            resolve(this.data);
        });
    });
}

loadUni(){
  if(this.universityData){
    return Promise.resolve(this.universityData);
}

return new Promise(resolve => {
        this.http.get('assets/data/unidata/universities.json').map(res => res.json()).subscribe(data => {
          this.universityData = data.university;
        resolve(this.universityData);
    });
   });
}

onDestroy(){
    this.newjason.unsubscribe();
}

}
