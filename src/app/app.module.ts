import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { ListPage } from '../pages/list/list';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Device } from '@ionic-native/device';
import { Data } from '../providers/data/data';
import { CalculatorPage } from '../pages/calculator/calculator';
import { MarkingProvider } from '../providers/marking/marking';
import { SMS } from '@ionic-native/sms';
import { ResultModalPage } from '../pages/result-modal/result-modal';
import { AngularFireModule} from 'angularfire2'
import { AngularFireDatabaseModule} from 'angularfire2/database'
import { FIREBASE_CONFIG } from './firebase.credentials';
import { genPinService } from '../pages/services/genPin.service';
import { IonicStorageModule } from '@ionic/storage';
import { AppState } from './app.global';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { feedbackService } from '../pages/services/feedback.service';


@NgModule({
  declarations: [
    MyApp,
    ListPage,
    ResultModalPage,
    CalculatorPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    SuperTabsModule.forRoot(),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ListPage,
    ResultModalPage,
    CalculatorPage
  ],
  providers: [
    SMS,
    AppState,
    StatusBar,
    Device,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Data,
    MarkingProvider,
    genPinService,
    feedbackService,
    InAppBrowser
  ]
})
export class AppModule {}
