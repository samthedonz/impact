import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { timer} from 'rxjs/observable/timer'
import { Storage } from '@ionic/storage';
import { AppState } from './app.global';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  nickname: string;
  email: string;
  rootPage: any = "WelcomePage";

  pages: Array<{ title: string, component: any, active: boolean, icon: string }>;
  navs: Array<{ title: string, component: any, active: boolean, icon: string }>;
  active: Array<{ title: string, component: any, active: boolean, icon: string }>;
  contact: Array<{ title: string, component: any, active: boolean, icon: string }>;
  settings: Array<{ title: string, component: any, active: boolean, icon: string }>;
  rightMenuItems: Array<{ icon: string, active: boolean }>;
  state: any;
  showSplash = true;
  placeholder = 'assets/img/avatar/girl-avatar.png';
  chosenPicture: any;

  constructor( public global: AppState,
    public menuCtrl: MenuController, public events: Events, storage: Storage, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    storage.get('Username').then ((val) => {
      console.log(val, 'nickname')
      this.nickname = val
    })
    storage.get('Email').then ((val) => {
      console.log(val, 'email')
      this.email = val
    })


    this.active = [
      { title: 'Enter Pin', component: 'ActivatePage', active: true, icon: 'key' },
      { title: 'Purchase Pin', component: 'PurchasePage', active: true, icon: 'cart'}
    ]
    this.navs = [
      { title: 'Home', component: 'WelcomePage', active: true, icon: 'home' },
    ];
    this.contact = [
      { title: 'Feedback', component: 'WebsitePage', active: false, icon: 'mail' },
      { title: 'About Impact Naija', component: 'ContactUsPage', active: true, icon: 'information-circle' },
      
    ]

    this.settings = [
      { title: "Developer's Profile", component: 'ProfilePage', active: true, icon: 'contact' },
      { title: 'Theming', component: 'ThemingPage', active: false, icon: 'power' }
    ]
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.global.set('theme', '');
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      timer(3000).subscribe(() => this.showSplash = false)
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
    // this.activePage.next(page);
  }
  open(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
    // this.activePage.next(page);
  }

}
