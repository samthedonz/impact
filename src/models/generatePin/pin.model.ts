
export interface Pin {
    key?: string;
    pinNumber: string;
    status?: boolean;
    phoneName?: string;
}