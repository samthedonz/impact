export interface feedback {
    key?: string;
    email: string;
    messageTitle: string;
    message: string;
}