import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'conductor',
  templateUrl: 'conductor.html'
})
export class ConductorComponent {


  text: string;

@Output() nextClick: EventEmitter<any>

  constructor() {
    console.log('Hello ConductorComponent Component');
    
    this.nextClick = new EventEmitter<any>();

  }

next(){
//  console.log("next clicked")
 this.nextClick.emit()
}

previous(){
  // this.pevClick.emit()
}

submit(){

  console.log("submit clicked")

}
}
