import { NgModule } from '@angular/core';
import {IonicModule} from 'ionic-angular';
import { ConductorComponent } from './conductor';

@NgModule({
	declarations: [
        ConductorComponent
    ],
	imports: [
        IonicModule
    ],
	exports: [
        ConductorComponent
    ]
})
export class ConductorComponentModule {}
