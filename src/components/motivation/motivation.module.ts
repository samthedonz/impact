import { NgModule } from '@angular/core';
import {IonicModule} from 'ionic-angular';
import { MotivationComponent } from './motivation';

@NgModule({
	declarations: [
        MotivationComponent
    ],
	imports: [
        IonicModule
    ],
	exports: [
        MotivationComponent
    ]
})
export class MotivationComponentModule {}
