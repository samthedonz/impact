import { Component, ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
@Component({
  selector: 'motivation',
  templateUrl: 'motivation.html'
})
export class MotivationComponent {
  @ViewChild('slider') slider: Slides;
  text: string;

  slides = [
    {
      title: 'Education is the passport to the future, for tomorrow belongs to those who prepare for it today.',
      imageUrl: 'assets/lists/img1.jpg',
      songs: 2,
      private: false
    },
    {
      title: 'An investment in knowledge pays the best interest',
      imageUrl: 'assets/lists/img2.png',
      songs: 4,
      private: false
    },
    {
      title: 'Develop a passion for learning. If you do, you will never cease to grow',
      imageUrl: 'assets/lists/img3.jpg',
      songs: 5,
      private: true
    },
    {
      title: 'Education is the foundation upon which we build our future',
      imageUrl: 'assets/lists/img4.jpg',
      songs: 12,
      private: true
    }
  ];

  constructor() {
    console.log('Hello MotivationComponent Component');
    this.text = 'Hello World';
  }

}
