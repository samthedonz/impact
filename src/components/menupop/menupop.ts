import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'menupop',
  templateUrl: 'menupop.html'
})
export class MenupopComponent {
  openMenu = false;

  text: string;

  constructor( public navCtrl: NavController) {
    console.log('Hello MenupopComponent Component');
    this.text = 'Hello World';
  }

  togglePopupMenu() {
    return this.openMenu = !this.openMenu;
  }

  goToBlog() {
   this.navCtrl.setRoot('WebsitePage')
  }

  goToHome() {
    this.navCtrl.setRoot('WelcomePage')
  }

  goToContact() {
    this.navCtrl.setRoot('ContactUsPage')
  }

  goToActivate() {
    this.navCtrl.push('ActivatePage')
  }

  goToProfile() {
    this.navCtrl.setRoot('ProfilePage')
  }

  activate() {
    this.navCtrl.setRoot('PurchasePage')
    
  }
}
