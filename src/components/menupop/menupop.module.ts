import { NgModule } from '@angular/core';
import {IonicModule} from 'ionic-angular';
import { MenupopComponent } from './menupop';

@NgModule({
	declarations: [
        MenupopComponent
    ],
	imports: [
        IonicModule
    ],
	exports: [
        MenupopComponent
    ]
})
export class MenupopComponentModule {}
