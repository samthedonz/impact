import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, Events} from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { MarkingProvider } from '../../providers/marking/marking';

@IonicPage()
@Component({
  selector: 'page-economics',
  templateUrl: 'economics.html',
})
export class EconomicsPage {
  @ViewChild('slides') slides: any;
	marking: boolean= false;
	myScore: number;
	marker: any;
	score = 0;
  	questions: any;
	getQuestion: any ;
	question: any;
	options: any;
	index = 1;
	Number: number = 1;
	university: any;
	quest;
	hasSubmitted: boolean = false;
	hasEntered = false;
	attempted = 0;
	underlined;
	continued;
	instruction;
	active = false;
	isSummited = false;

	constructor(public events: Events, public http: Http, public popoverCtrl: PopoverController, public navCtrl: NavController, public navParams: NavParams, public mark: MarkingProvider) {
		this.university = this.navParams.get('uni')
		this.mark.totalScore = 0;

		events.subscribe('nextclicked', () => {
			if (this.hasEntered) {
				this.next()
			}
		  });
		
		  events.subscribe('pevclicked', () => {
			if (this.hasEntered) {
				this.previous()
			}
		  });
		  events.subscribe('submitclicked', () => {
				this.submit()
				this.isSummited = true;
		  });
  }

  randomizeAnswers(rawAnswers: any[]): any[] {

	for (let i = rawAnswers.length - 1; i > 0; i--) {
		let j = Math.floor(Math.random() * (i + 1));
		let temp = rawAnswers[i];
		rawAnswers[i] = rawAnswers[j];
		rawAnswers[j] = temp;
	}
	return rawAnswers;
}

  ngOnInit() {
	this.http
	.get('assets/data/'+this.university+'/economics/questions.json')
	.map(res => res.json())
	.subscribe(data => {
	 if (this.navParams.get('shuffleQus') == 'On') {
		let originalOrder = data.questions;
		data.questions = this.randomizeAnswers(originalOrder);
	 }
	 if (this.navParams.get('shuffleAns') == 'On') {
		for (let index = 0; index < data.questions.length; index++) {
			data.questions[index].answers.forEach(element => {
			  let originalOrder = data.questions[index].answers;
			  data.questions[index].answers = this.randomizeAnswers(originalOrder)
		  });
		}
	 }	  
			this.quest = data
			this.questions = this.quest.questions.slice(0,this.navParams.get('questionCount'))
			this.getQuestion = this.questions

			this.question = this.getQuestion[0].question
			if (this.getQuestion[0].underlined != false) {
			this.underlined = this.getQuestion[0].underlined
			}else{
				this.underlined = ""
			}
			if (this.getQuestion[0].instruction != false) {
				this.instruction = this.getQuestion[0].instruction
				}else {
					this.instruction = ""
				}
			if (this.getQuestion[0].continued != false) {
				this.continued = this.getQuestion[0].continued
				}else {
					this.continued = ""
				}
			this.options = this.getQuestion[0].answers
			if (this.Number == this.Number ) {
				this.active = true;
			}
			else {
				this.active = false
			}
	})
	this.mark.load().then((data) => {
		data.map((subjects) => {
			return subjects
		});		
		this.marker = data;
		this.marking = this.marker[5].isSubmitted = false;
		this.myScore = this.marker[5].score = 0
	});
  }
	submit(){
		for (var i of this.getQuestion) {
		if (i.correctAnswer == 'true') {
			this.score++
			}	
		if (i.hasAnswered == true)
			{
				this.attempted++
			}
	}
		this.hasSubmitted = true;		
		this.marking = this.marker[5].isSubmitted = true;
		this.myScore = this.marker[5].score = this.score;
		this.marker[5].attempted = this.attempted
		this.mark.totalScore = this.mark.totalScore + this.score
	}
	
	ionViewWillEnter(){
	this.hasEntered = true
	}

	ionViewWillLeave(){
		
	this.hasEntered = false
	}

	nextSlide(i){
			this.question = this.getQuestion[i].question
			if (this.getQuestion[i].underlined != false) {
				this.underlined = this.getQuestion[i].underlined
				}else{
					this.underlined = ""
				}
				if (this.getQuestion[i].instruction != false) {
					this.instruction = this.getQuestion[i].instruction
					}
				else{
					this.instruction = ""
				}
				if (this.getQuestion[i].continued != false) {
					this.continued = this.getQuestion[i].continued
					}
				else {
					this.continued = ""
				}
			this.options = this.getQuestion[i].answers
			this.Number = i+1
	}

	next(){
		
		try {
			
			let a = this.Number
			this.question = this.getQuestion[a].question
			if (this.getQuestion[a].underlined != false) {
				this.underlined = this.getQuestion[a].underlined
				}else{
					this.underlined = ""
				}
				if (this.getQuestion[a].instruction != false) {
					this.instruction = this.getQuestion[a].instruction
					}
				else{
					this.instruction = ""
				}
				if (this.getQuestion[a].continued != false) {
					this.continued = this.getQuestion[a].continued
					}
				else {
					this.continued = ""
				}
			this.options = this.getQuestion[a].answers
			this.Number = this.Number + 1
		} catch (error) {	
	}	
}
	previous(){
		try {
			let a = this.Number - 1
			this.question = this.getQuestion[a-1].question
			if (this.getQuestion[a-1].underlined != false) {
				this.underlined = this.getQuestion[a-1].underlined
				}else{
					this.underlined = ""
				}
				if (this.getQuestion[a-1].instruction != false) {
					this.instruction = this.getQuestion[a-1].instruction
					}
				else{
					this.instruction = ""
				}
				if (this.getQuestion[a-1].continued != false) {
					this.continued = this.getQuestion[a-1].continued
					}
				else {
					this.continued = ""
				}
			this.options = this.getQuestion[a-1].answers
			this.Number = this.Number - 1

		} catch (error) {	
		}
	}

	selectAnswer(question, answer, i, index){
		let selected = this.getQuestion[this.Number-1].selectedAnswer = i
		this.getQuestion[this.Number-1].hasAnswered = true 
		switch (selected) {
			case 0:
			this.getQuestion[this.Number-1].answers[1].selected = false
			this.getQuestion[this.Number-1].answers[2].selected = false 
			this.getQuestion[this.Number-1].answers[3].selected = false 
			this.getQuestion[this.Number-1].answers[0].selected = true
				break;
			case 1:
			this.getQuestion[this.Number-1].answers[1].selected = true
			this.getQuestion[this.Number-1].answers[2].selected = false 
			this.getQuestion[this.Number-1].answers[3].selected = false 
			this.getQuestion[this.Number-1].answers[0].selected = false
				break;
			case 2:
			this.getQuestion[this.Number-1].answers[1].selected = false
			this.getQuestion[this.Number-1].answers[3].selected = false 
			this.getQuestion[this.Number-1].answers[2].selected = true 
			this.getQuestion[this.Number-1].answers[0].selected = false
				break;
			case 3:
			this.getQuestion[this.Number-1].answers[1].selected = false
			this.getQuestion[this.Number-1].answers[3].selected = true 
			this.getQuestion[this.Number-1].answers[2].selected = false
			this.getQuestion[this.Number-1].answers[0].selected = false
				break;
				default:
				break;
		}
		
		if (this.getQuestion[this.Number-1].answers[i].correct == true) {
			this.getQuestion[this.Number-1].correctAnswer = "true"
		} else {
			this.getQuestion[this.Number-1].correctAnswer = "false"
		}

	}
}
