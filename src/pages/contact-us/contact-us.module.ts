import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactUsPage } from './contact-us';
import { MenupopComponentModule } from '../../components/menupop/menupop.module';

@NgModule({
  declarations: [
    ContactUsPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactUsPage),
    MenupopComponentModule
  ],
})
export class ContactUsPageModule {}
