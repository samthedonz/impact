import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-profile-five',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  user = {
    name: 'Lawal Samuel',
    profileImage: 'assets/img/avatar/ian-avatar.png',
    coverImage: 'assets/img/background/background-5.jpg',
    occupation: 'Mobile/Web',
    location: 'Ibadan, Oyo State',
    description: 'Passionate Designer. Recently focusing on developing mobile hybrid apps and web development.',
    address: 'Shop 5, Opposite Bello Hall, University of Ibadan',
    phone: '+2349052085121',
    email: 'samthedonz@gmail.com',
    whatsapp: '09052085121',
  };

  constructor(public navCtrl: NavController) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
