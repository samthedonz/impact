import { ParallaxHeaderDirectiveModule } from '../../components/parallax-header/parallax-header.module';
import { ProfilePage } from './profile';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenupopComponentModule } from '../../components/menupop/menupop.module';

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    ParallaxHeaderDirectiveModule,
    MenupopComponentModule
  ],
  exports: [
    ProfilePage
  ]
})

export class ProfilePageModule { }
