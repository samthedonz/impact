import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Events, AlertController } from 'ionic-angular';
import { MarkingProvider } from '../../providers/marking/marking';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  uni;
  subjectSel: string;
  mode: any = "Practice";
  selectedUni: any;
  number: number[] = [5,10,15];
  questionCount : number = 5;
  min: number = 5;
  hours: number = 0;
  marker: any;
  time;
  shuffleQus = "Off";
  shuffleAns = "Off";
  activated = '';
  shuffles = false;
  constructor(public alert: AlertController, private storage: Storage, public events: Events, platform: Platform, public mark: MarkingProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.uni = this.navParams.get('name')
    this.selectedUni = this.navParams.get('subjects')
    events.unsubscribe('yesClicked')
    this.storage.get('status').then ((val) => {
      this.activated = val
    console.log(this.activated)
      if (this.activated == "Activated" ) {
        this.shuffles = true
        this.shuffleQus = "On"
        this.shuffleAns = "On"
      }
    }) 
  }
 
  ionViewWillEnter(){
    this.mark.load().then((data) => {
      data.map((subjects) => {
        return subjects
      });		
      this.marker = data;
      for (var i of this.marker) {
        i.isSubmitted = false
        console.log(i)
      }
    })
    console.log(this.marker)
  }

  ionViewWillLoad() {
    
  }

  pushContainer() {
  try {
    let data = {
      university: this.navParams.get('nickname'),
      subject: this.subjectSel,
      number: this.questionCount,
      shuffleQus: this.shuffleQus,
      shuffleAns: this.shuffleAns,
      time: this.min*60 + this.hours*3600
    }
      console.log(this.navParams.get('nickname'))
     this.navCtrl.push('ContainerPage', data)
  
  } catch (error) {
    let alert = this.alert.create({
      title: 'InComplete Inputs',
      subTitle: 'Please Select Correct Values',
      buttons: ['Dismiss']
    });
    alert.present();
  }
      
      
     
      
   
  }
}
