import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/**
 * Generated class for the TestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class TestPage {
data: any;
university = "ui";

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }
  
  ionViewDidLoad() {
    this.http.get('assets/data/'+this.university+'/english/questions.json').map(res => res.json()).subscribe(data => {
      console.log(data);
      this.data = data
  })
}
getmyQuestions(){
  console.log(this.data.questions[1].question)
}

getmyAnswers(){
  console.log(this.data.questions[1].answers[0].correct)
  this.data.questions[1].answers[0].correct = true;
}

}