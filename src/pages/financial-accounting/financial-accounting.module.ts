import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinancialAccountingPage } from './financial-accounting';

@NgModule({
  declarations: [
    FinancialAccountingPage,
  ],
  imports: [
    IonicPageModule.forChild(FinancialAccountingPage),
  ],
})
export class FinancialAccountingPageModule {}
