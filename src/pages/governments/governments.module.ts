import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GovernmentsPage } from './governments';

@NgModule({
  declarations: [
    GovernmentsPage,
  ],
  imports: [
    IonicPageModule.forChild(GovernmentsPage),
  ],
})
export class GovernmentsPageModule {}
