import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PinGenPage } from './pin-gen';

@NgModule({
  declarations: [
    PinGenPage,
  ],
  imports: [
    IonicPageModule.forChild(PinGenPage),
  ],
})
export class PinGenPageModule {}
