import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Pin } from '../../models/generatePin/pin.model';
import {genPinService } from '../services/genPin.service';
 

@IonicPage()
@Component({
  selector: 'page-pin-gen',
  templateUrl: 'pin-gen.html',
})
export class PinGenPage {

  pin: Pin = {
    pinNumber: '',
    status: false,
    phoneName: ''
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, private genPin: genPinService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PinGenPage');
  }

  addPin(pin: Pin) {
    this.genPin.addPin(pin).then(ref =>{
    console.log(ref.key)
    })
  }

  goBack(){
    this.navCtrl.push('ActivatePage')
  }
}
