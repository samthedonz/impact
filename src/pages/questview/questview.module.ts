import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestviewPage } from './questview';
import { SuperTabsModule } from 'ionic2-super-tabs';
// import { FlashCardComponent } from '../components/flash-card/flash-card';

@NgModule({
  declarations: [
    QuestviewPage,
  ],
  imports: [
    SuperTabsModule,
    
    IonicPageModule.forChild(QuestviewPage),
  ],
})
export class QuestviewPageModule {}
