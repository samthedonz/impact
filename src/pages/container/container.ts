import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ModalController, ViewController } from 'ionic-angular';
import { CalculatorPage } from '../calculator/calculator';
import { ViewChild} from '@angular/core';
import { Timer } from '../../components/countdown-timer/timer';
import { Data } from '../../providers/data/data';
import { Events } from 'ionic-angular';
import { ResultModalPage } from '../result-modal/result-modal';
import { Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Navbar } from 'ionic-angular';
import { MarkingProvider } from '../../providers/marking/marking';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-container',
  templateUrl: 'container.html',
})
export class ContainerPage {
  @ViewChild(Timer) timer: Timer;
  @ViewChild(Navbar) navBar: Navbar;
  tab2Params = {  questionCount: 0, 
                  uni:"",
                  shuffleQus: "Off",
                  shuffleAns: "Off",      
        };
    number: any;
    hideSubmitButton = false;
    activeTabIndex;
    myData;
    selected: string[];
    subject = [];
    score = [];
    time;
    activated: string;
    hasEntered: false;

ionViewWillEnter(){
  this.score = []
}

review(){
  let myData
  this.myData = myData = {
    score: this.score,
    subject: this.selected,
    totalScore: this.number
  } 
  let myResultModal = this.Modal.create(ResultModalPage, myData )
  myResultModal.present();
  this.score = []   
  this.hideSubmitButton = true
}

  constructor(  private storage: Storage,
                public events: Events,
                private viewCtrl: ViewController, 
                public navCtrl: NavController, 
                public navParams: NavParams, 
                public popoverCtrl: PopoverController, 
                public dataService: Data,
                public Modal: ModalController,
                public platform: Platform,
                private alertCtrl: AlertController,
                private mark: MarkingProvider,
              ) 
  {
    this.selected = this.navParams.get('subject');

    this.storage.get('status').then ((val) => {
      this.activated = val
      console.log(this.activated, 'asdasdf')
      if (this.activated == 'Not-Activated' ) {
        
        this.alertCtr()
      }
    
    }) 


    console.log(this.selected.length, "selected");
    this.mark.numSubject = this.selected.length;
    this.number = this.navParams.get('number');
    this.tab2Params.questionCount = this.number
     this.mark.numQuestionpersub = this.number
    this.tab2Params.uni = this.navParams.get('university')
    this.tab2Params.shuffleAns = this.navParams.get('shuffleAns')
    this.tab2Params.shuffleQus = this.navParams.get('shuffleQus')
    const index = this.viewCtrl.index;
    console.log(index, "my index")
    this.time =  this.navParams.get('time')
    events.subscribe('timeUp', () => {
			this.submit()
      });


      this.navi()
  }
  alertCtr(){
    let alert = this.alertCtrl.create({
      title: 'Product Not Activated',
      message: 'You are allowed to answer at most 5 questions per subject. Click PURCHASE to obtain pin or call:- 08141159951',
      buttons: [
        {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
        },
        {
        text: 'PURCHASE',
        handler: () => {
          this.navCtrl.push('PurchasePage')
        }
        }
      ]
      });
      alert.present();
    }
  ionViewDidEnter(){

  }

  navi(){
    if (this.ionViewWillLoad) {
      let backAction = this.platform.registerBackButtonAction(() => {
        this.timer.pauseTimer();
        // this.events.unsubscribe('nextclicked')
        // this.events.unsubscribe('pevclicked')
        this.alert()
      backAction();
    },1);
  }
  }
  alert(){
    let alert = this.alertCtrl.create({
      title: 'Quit',
      message: 'Are you sure you want to Quit Exam',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.timer.resumeTimer();
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.navCtrl.pop()
            this.events.publish('yesClicked')
          }
        }
      ]
    });
    alert.present();
  }
  tabSelected(event){
    this.activeTabIndex = event;
    console.log(this.activeTabIndex, 'changed')
  }

  ngOnInit() {
    setTimeout(() => {
      this.timer.startTimer();
    }, 1000)
    
  }

  ionViewDidLoad(){
    this.navBar.backButtonClick = (e:UIEvent)=>{
      this.timer.pauseTimer();
      let alert = this.alertCtrl.create({
        title: 'Quit',
        message: 'Are you sure you want to Quit Exam',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              this.timer.resumeTimer();
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.navCtrl.pop()
              this.events.publish('yesClicked')
            }
          }
        ]
      });
      alert.present();
    }
  }
  ionViewDidLeave(){
    console.log("leaft")
    this.events.unsubscribe('timeUp')
    this.events.unsubscribe('submitted')
  }
  
  submit(){
    this.timer.pauseTimer()
    this.events.publish('submitclicked')
    let myData
    this.events.subscribe('submitted', (score) => {
    this.score.push(score)
    })    
        this.myData = myData = {
        score: this.score,
        subject: this.selected,
        totalScore: this.number
      } 
      let myResultModal = this.Modal.create(ResultModalPage, myData )
      myResultModal.present();
      this.score = []   
      this.hideSubmitButton = true
  }  
  next(){
    this.events.publish('nextclicked')
  }
  previous(){
    this.events.publish('pevclicked')
  }
  ionViewWillLoad() {
    this.selected = this.navParams.get('subject');
  }
  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(CalculatorPage);
    popover.present({
      ev: myEvent
    });
  }
}