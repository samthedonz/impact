import { NgModule} from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContainerPage } from './container';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { TimerModule } from '../../components/countdown-timer/timer.module';
import { ConductorComponentModule } from '../../components/conductor/conductor.module';


@NgModule({
  declarations: [
    ContainerPage,
   
  ],
  imports: [
    SuperTabsModule.forRoot(),
    IonicPageModule.forChild(ContainerPage),
    TimerModule,
    ConductorComponentModule
  ],
})
export class ContainerPageModule {}
