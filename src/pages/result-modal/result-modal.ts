import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import { MarkingProvider } from '../../providers/marking/marking';
import chartJs from 'chart.js';


@Component({
  selector: 'page-result-modal',
  templateUrl: 'result-modal.html',
})
export class ResultModalPage {

  @ViewChild('pieCanvas') pieCanvas;
  barChart: any;
  pieChart: any;
  totalScore: number;
  subject= [];
  scores = [];
  getAllScores= [];
  displayedsubject = []
  marker = [];
  questionperSub = 0;
  total = 0;
  percentage;
  constructor(public http: Http, public mark: MarkingProvider, public events: Events, public navCtrl: NavController, public navParams: NavParams, public view: ViewController) { 
    this.totalScore = 0
    this.totalScore = this.mark.totalScore
    events.subscribe('yesClicked', () => {
      this.view.dismiss()
    })
    this.questionperSub = this.mark.numQuestionpersub
    this.total = this.mark.numQuestionpersub * this.mark.numSubject
    this.percentage = ((this.totalScore/this.total)*100).toFixed(2)
  }
  getChart(context, chartType, data, options?) {
    return new chartJs(context, {
      data,
      options,
      type: chartType,
    });
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.pieChart = this.getPieChart();
    }, 350);
}
restart(){
  this.view.dismiss()
}

  closeModal() {
      this.view.dismiss()
  }

  ionViewWillLoad() {
    
    this.subject = this.navParams.get('subject')
    console.log(this.subject)
      this.mark.load().then((data) => {
        data.map((subjects) => {
          return subjects
        });		
        this.marker = data;
        for (let i = 0; i < this.subject.length; i++) {
          for (var j of this.marker) {
            if (j.name == this.subject[i]) {
            this.getAllScores.push(j)
            console.log(this.getAllScores)
            let getnew = []
            getnew.push(this.getAllScores[0].score)
            }
          } 
        }
      });
      this.events.unsubscribe('submitclicked')
  }
  
getPieChart() {
  
    let name = []
    let score = []
    name.push('wrong')
    score.push(this.total-this.totalScore)
    for (let i = 0; i < this.getAllScores.length; i++) {
      name.push(this.getAllScores[i].name);
      score.push(this.getAllScores[i].score)
    }
    const data = {
      labels: name,
      datasets: [
        {
          data: score,
          backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56','#2B3A67','#B56B45','#FF9B71','#49306B','#ACE4AA', '#084C61', '#010001','#874000','#9B287B'],
          hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56','#2B3A67','#B56B45','#FF9B71','#49306B','#ACE4AA', '#084C61', '#010001','#874000','#9B287B']
        }]
    };
    return this.getChart(this.pieCanvas.nativeElement, 'pie', data);
  }

getPieChart2() {
    let name = []
    let score = []
    for (let i = 0; i < this.getAllScores.length; i++) {
      name.push(this.getAllScores[i].name);
      score.push(this.getAllScores[i].score)
    }
    const data = {
      labels: ["Got Right", "Got Wrong"],
      datasets: [
        {
          data: [score,this.navParams.get('totalScore')],
          backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
          hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56']
        }]
    };
    return this.getChart(this.pieCanvas.nativeElement, 'pie', data);
}
  
}
