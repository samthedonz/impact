import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Data } from '../../providers/data/data';

@IonicPage()
@Component({
  selector: 'page-select-uni',
  templateUrl: 'select-uni.html',
})

export class SelectUniPage {
 university: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public dataService: Data) {
   
		this.dataService.loadUni().then((data) => {

			data.map((university) => {
	      		return university
	    	});		
      this.university = data;
      console.log(this.university)
		});	
  }

  ionViewDidLoad() {
    

  }

  pushPage(uni) {
    let universityData = uni
    this.navCtrl.push('SettingsPage', universityData)
    console.log(universityData)
  }
}
