import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectUniPage } from './select-uni';

@NgModule({
  declarations: [
    SelectUniPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectUniPage),
  ],
})
export class SelectUniPageModule {}
