import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SmartBlogPage } from './smart-blog';
import { MenupopComponentModule } from '../../components/menupop/menupop.module';


@NgModule({
  declarations: [
    SmartBlogPage,
  ],
  imports: [
    IonicPageModule.forChild(SmartBlogPage),
    MenupopComponentModule
  ],
})
export class SmartBlogPageModule {}
