import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GovernmentPage } from './government';

@NgModule({
  declarations: [
    GovernmentPage,
  ],
  imports: [
    IonicPageModule.forChild(GovernmentPage),
  ],
})
export class GovernmentPageModule {}
