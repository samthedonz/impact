import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrsPage } from './crs';

@NgModule({
  declarations: [
    CrsPage,
  ],
  imports: [
    IonicPageModule.forChild(CrsPage),
  ],
})
export class CrsPageModule {}
