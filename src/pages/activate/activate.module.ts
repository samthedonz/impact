import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivatePage } from './activate';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenupopComponentModule } from '../../components/menupop/menupop.module';

@NgModule({
  declarations: [
    ActivatePage,
  ],
  imports: [
    IonicPageModule.forChild(ActivatePage),
    FormsModule,
    ReactiveFormsModule,
    MenupopComponentModule
  ],
})
export class ActivatePageModule {}
