import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Observable } from 'rxjs';
import { Pin } from '../../models/generatePin/pin.model';
import { genPinService } from '../services/genPin.service';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-activate',
  templateUrl: 'activate.html',
})
export class ActivatePage {
details1 = "detaisl";
details2  = "detaisl" ;
pin;
phoneNumber;
productGen;
enteredPin: number;
activated = false;

nickname: string;
email: string;
genPin$: Observable<Pin[]>

  constructor(private alertCtrl: AlertController, private storage: Storage, private gen: genPinService, public navCtrl: NavController, public navParams: NavParams) {
      this.genPin$ = this.gen.getGenPin().snapshotChanges().map( changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }))
      }) 
      // this.storage.set('status','Not-Activated');
  }

  ionViewWillLoad(){
    this.storage.get('status').then ((val) => {
      console.log(val)
      if (val == 'Activated') {
        this.activated = true
         
      } else {
        this.activated = false
        let alert = this.alertCtrl.create({
          title: 'Important ',
          subTitle: 'You need internet connection for this one time activation. Thank You.',
          buttons: ['Dismiss']
        });
        alert.present();
      }
    })
  }

  ionViewDidLoad(){
    
  }
  

 async passData(pin){
    try {
    await  this.storage.set('Username', this.nickname)
    this.storage.set('Email', this.email)
      pin.status = true;
      this.gen.editPin(pin)
      this.storage.set('status','Activated');
      this.navCtrl.pop();
      this.navCtrl.push('ActivatePage')  
    } catch (error) {
      console.log(error)
      let alert = this.alertCtrl.create({
        title: 'Activation failed ',
        subTitle: 'You need internet connection for this one time activation. Thank You.',
        buttons: ['Dismiss']
      });
      alert.present();
    }
    
  }

  sendSMS(){

  }

}
