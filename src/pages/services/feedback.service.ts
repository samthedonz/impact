import { Injectable } from "@angular/core";
import {AngularFireDatabase } from "angularfire2/database";
import { feedback} from "../../models/feedback/feedback.model";


@Injectable()
export class feedbackService{

    private feedbackRef = this.db.list<feedback>('feedbacks');

    constructor(private db: AngularFireDatabase){

    }
    getfeedback() {
        return this.feedbackRef;
    }

    sendfeedback (feedback: feedback) {
        return this.feedbackRef.push(feedback)
    }

    // editfeedback(feedback: feedback){
    //     return this.feedbackRef.update(feedback.pin, pin)
    // }

}