import { Injectable } from "@angular/core";
import {AngularFireDatabase } from "angularfire2/database";
import { Pin } from "../../models/generatePin/pin.model";
// import { Pin } from "../../models/generatePin/pin.model";

@Injectable()
export class genPinService{

    private genPinRef = this.db.list<Pin>('genPin');

    constructor(private db: AngularFireDatabase){

    }
    getGenPin() {
        return this.genPinRef;
    }

    addPin (pin: Pin) {
        return this.genPinRef.push(pin)
    }

    editPin(pin: Pin){
        return this.genPinRef.update(pin.key, pin)
    }

}