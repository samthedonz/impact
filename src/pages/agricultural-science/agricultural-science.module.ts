import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgriculturalSciencePage } from './agricultural-science';

@NgModule({
  declarations: [
    AgriculturalSciencePage,
  ],
  imports: [
    IonicPageModule.forChild(AgriculturalSciencePage),
  ],
})
export class AgriculturalSciencePageModule {}
