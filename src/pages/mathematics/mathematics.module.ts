import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MathematicsPage } from './mathematics';

@NgModule({
  declarations: [
    MathematicsPage,
  ],
  imports: [
    IonicPageModule.forChild(MathematicsPage),
  ],
})
export class MathematicsPageModule {}
