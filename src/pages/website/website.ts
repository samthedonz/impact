import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { feedback } from '../../models/feedback/feedback.model';
import { feedbackService } from '../services/feedback.service';


@IonicPage()
@Component({
  selector: 'page-website',
  templateUrl: 'website.html',
})


export class WebsitePage {
  @ViewChild('myInput') myInput: ElementRef;
  myStuff: any;

  feedback: feedback = {
    email: '',
    messageTitle: '',
    message: '',
  }
  constructor(private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, private feed: feedbackService) {
  }

   async send(feed) {
    try {
    await  this.feed.sendfeedback(feed).then(ref =>{
        console.log(ref.key)
          let alert = this.alertCtrl.create({
            title: 'Message Sent',
            subTitle: "We have received your feedback. Thank's for your surport",
            buttons: ['Dismiss']
          });
          alert.present();
        
        })
        this.navCtrl.setRoot('WelcomePage')
    } catch (error) {
      console.log(error)
          let alert = this.alertCtrl.create({
            title: 'Failed',
            subTitle: "Please Check your Internet Connection",
            buttons: ['Dismiss']
          });
          alert.present();
        
        }
   
  }

  // goBack(){
  //   this.navCtrl.push('ActivatePage')
  // }
  resize() {
    this.myInput.nativeElement.style.height = this.myInput.nativeElement.scrollHeight + 'px';
}
  ionViewWillLoad(){

    
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad WebsitePage');
  }

}
