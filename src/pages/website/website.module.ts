import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WebsitePage } from './website';
import { MenupopComponentModule } from '../../components/menupop/menupop.module';

@NgModule({
  declarations: [
    WebsitePage,
  ],
  imports: [
    IonicPageModule.forChild(WebsitePage),
    MenupopComponentModule
  ],
})
export class WebsitePageModule {}
