import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PurchasePage } from './purchase';
import { MenupopComponentModule } from '../../components/menupop/menupop.module';

@NgModule({
  declarations: [
    PurchasePage,
  ],
  imports: [
    IonicPageModule.forChild(PurchasePage),
    MenupopComponentModule
  ],
})
export class PurchasePageModule {}
