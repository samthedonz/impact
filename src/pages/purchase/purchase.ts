import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { SMS } from '@ionic-native/sms';

@IonicPage()
@Component({
  selector: 'page-purchase',
  templateUrl: 'purchase.html',
})
export class PurchasePage {

  cardTypes: any;
  phoneNumber: any;
  pin: any;

  constructor(public sms: SMS, public navCtrl: NavController, public navParams: NavParams, private toast: ToastController) {

  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad PurchasePage');
  }

  async sendSMS(){
  try {
      await this.sms.send('07060731505', "CARD TYPE: "+ this.cardTypes+" "+"PIN: "+this.pin+"Phone-Number: "+this.phoneNumber);
      let toast = this.toast.create({
        message: 'Message Sent Successfully',
        duration: 3000,
        position: 'top'
      });
      toast.present();
        this.navCtrl.push('ActivatePage')
    }catch(e){
      let toast = this.toast.create({
        message: 'Set Default Sim for Messaging in Settings or check Your Account Balance',
        duration: 5000,
        position: 'top'
      });
      toast.present();

    }
  }
}
