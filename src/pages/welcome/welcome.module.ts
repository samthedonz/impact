import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WelcomePage } from './welcome';
import { MotivationComponentModule } from '../../components/motivation/motivation.module';
import { MenupopComponentModule } from '../../components/menupop/menupop.module';


@NgModule({
  declarations: [
    WelcomePage,
  ],
  imports: [
    IonicPageModule.forChild(WelcomePage),
    MotivationComponentModule,
    MenupopComponentModule
  ],
})
export class WelcomePageModule {}
